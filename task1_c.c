#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define ERR_MSG "Wrong format, try again."
#define MAX 20

bool userInputIsValid(char entry[]);
bool ageIsValid(int age);
	
int main(){
	char name[MAX], lastName[MAX], nameOlder[MAX], nameYounger[MAX], currentName[MAX];
	int age, sumOfAges=0, averageAge, maxAge=0, minAge=80;

	for(int i=1 ; i<11 ; i++){
		do{
			printf("Enter the name [%d] : ",i);
			scanf("%s",name);
		}while(!userInputIsValid(name));
		/* i didn't passed any value to the function 
	       	   and c didn't tell me any error, just went
		   out of the loop */

		do{
			printf("Enter the last name [%d] : ",i);
			scanf("%s",lastName);
		}while(!userInputIsValid(lastName));

		 do{
		        printf("Enter the age [%d] : ",i);
		        scanf("%d",&age);
		}while(!ageIsValid(age));
		/* if you type a character, the loop continues infinitely ,why?! */
		
		sumOfAges+=age;
		strcpy(currentName,name);
		strcat(currentName," ");
		strcat(currentName,lastName);

		if(age>maxAge){
			maxAge=age;
			strcpy(nameOlder,currentName);
		}

		if(age<minAge){
			minAge=age;
			strcpy(nameYounger,currentName);
		}

	}
	
	printf("The average age is %.1f \n", ((float)sumOfAges)/10);
	printf("The older one is %s \n",nameOlder);
	printf("The younger one is %s \n",nameYounger);

}

bool userInputIsValid(char entry[]){
	
	if (!isupper(entry[0])){
		printf("%s",ERR_MSG);
		return false; 
	}
	
	for(int i=1 ; i<strlen(entry) ;i++){
		/* i could use isalpha but i did the below for learning purposes */
                if(entry[i]<65 ||(entry[i]>90 && entry[i]<97)|| entry[i]>122){
			printf("%s",ERR_MSG);
                        return false;
                }
	}
	return true;        
}

bool ageIsValid(int age){
	if(age<16 || age>80){
		printf("%s",ERR_MSG);
		return false;
	}
	return true;

}
